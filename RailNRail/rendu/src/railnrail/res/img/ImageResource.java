package railnrail.res.img;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 * Image resources
 * 
 * @author Lucas SOARES
 */
public enum ImageResource
{
	/**
	 * Image list
	 */
	IMAGE_PAUSE,
	IMAGE_PLAY,
	
	IMAGE_FAST_FORWARD,
	IMAGE_FAST_FORWARD2,
	IMAGE_FAST_FORWARD3,
	IMAGE_FAST_FORWARD4,
	IMAGE_FAST_FORWARD5,
	IMAGE_FAST_FORWARD6,
	
	IMAGE_GREEN_LIGHT,
	IMAGE_RED_LIGHT,
	IMAGE_INCIDENT_LIGHT_1,
	IMAGE_INCIDENT_LIGHT_2,
	
	IMAGE_LEFT,
	IMAGE_RIGHT,
	
	IMAGE_RAIL_END_LEFT,
	IMAGE_RAIL_END_RIGHT,
	IMAGE_RAIL_HORIZONTAL,
	IMAGE_RAIL_LB,
	IMAGE_RAIL_LT,
	IMAGE_RAIL_TR,
	IMAGE_RAIL_BR,
	IMAGE_RAIL_HLB,
	
	IMAGE_STATION,
	IMAGE_TRAIN,
	IMAGE_TRAIN_SELECTED,
	
	IMAGE_EMPTY_CELL,
	IMAGE_EMPTY_CELL_2,
	IMAGE_EMPTY_CELL_3,
	IMAGE_EMPTY_CELL_4,
	
	IMAGE_STATION_CELL_EMPTY,
	IMAGE_STATION_CELL_BOTTOM,
	IMAGE_STATION_CELL_TOP,
	
	IMAGE_RAIL_VERTICAL_SMALL,
	
	IMAGE_INCIDENT_ICON,
	
	IMAGE_TRAIN_ICON;
	
	/**
	 * Images names
	 */
	public static final String[ ] Name =
	{
		"pause.png",
		"play.png",
		
		"fast_forward.png",
		"fast_forward2.png",
		"fast_forward3.png",
		"fast_forward4.png",
		"fast_forward5.png",
		"fast_forward6.png",
		
		"GreenLight.png",
		"RedLight.png",
		"IncidentLight1.png",
		"IncidentLight2.png",
		"Left.png",
		"Right.png",
		
		"rail_end_left.png",
		"rail_end_right.png",
		"rail_H.png",
		"Rail_LB.png",
		"Rail_LT.png",
		"Rail_TR.png",
		"Rail_BR.png",
		"Rail_HLB.png",
		
		"station.png",
		"train.png",
		"trainSelected.png",
		
		"EmptyCell.png",
		"EmptyCell2.png",
		"EmptyCell3.png",
		"EmptyCell4.png",
		
		"EmptyCell.png",
		"StationCell.png",
		"StationCellTop.png",
		
		"RailVP.png",
		
		"incident.png",
		
		"trainIcon.png"
	};
	
	/**
	 * Get image URL
	 * 
	 * @param image
	 * 		The image
	 * 
	 * @return the url
	 */
	public static URL getImageURL( ImageResource image )
	{
		return ImageResource.class.getResource( ImageResource.Name[ image.ordinal( ) ] );
	}
	
	/**
	 * Get image stream
	 * 
	 * @param image
	 * 		The image to get stream of
	 * 
	 * @return the stream
	 */
	public static InputStream getImageStream( ImageResource image )
	{
		return ImageResource.class.getResourceAsStream( ImageResource.Name[ image.ordinal( ) ] );
	}
	
	/**
	 * Load image
	 * 
	 * @param image
	 * 		The image to be loaded
	 * 
	 * @return the image
	 * 
	 * @throws IOException
	 */
	public static BufferedImage loadImage( ImageResource image ) throws IOException
	{
		return ImageIO.read( getImageStream( image ) );
	}
	
	/**
	 * Load icon
	 * 
	 * @param image
	 * 		The image to be loaded
	 * 
	 * @return the icon
	 * 
	 * @throws IOException
	 */
	public static ImageIcon loadIcon( ImageResource image ) throws IOException
	{
		return new ImageIcon( loadImage( image ) );
	}
}
