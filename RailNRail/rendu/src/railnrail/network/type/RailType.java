package railnrail.network.type;

/**
 * Rail type
 * 
 * @author Lucas SOARES
 */
public enum RailType
{
	RAIL_END_LEFT,
	RAIL_END_RIGHT,
	RAIL_HORIZONTAL,
	RAIL_LEFT_TO_BOTTOM,
	RAIL_LEFT_TO_TOP,
	RAIL_TOP_TO_RIGHT,
	RAIL_BOTTOM_TO_RIGHT,
	RAIL_HORIZONTAL_AND_LEFT_TO_BOTTOM;
	
	/**
	 * Keyword for rail type
	 */
	public static final String[ ] Keyword =
	{
		"EL",
		"ER",
		"H",
		"LTB",
		"LTT",
		"TTR",
		"BTR",
		"HLB"
	};
	
	/**
	 * Get type from keyword
	 * 
	 * @param keyword
	 * 		The keyword
	 * 
	 * @return the rail type
	 */
	public static RailType FindRailType( String keyword )
	{
		// Look for given keyword
		for( RailType type : RailType.values( ) )
			if( Keyword[ type.ordinal( ) ].equals( keyword ) )
				return type;
		
		// Can't find
		return null;
	}
}
