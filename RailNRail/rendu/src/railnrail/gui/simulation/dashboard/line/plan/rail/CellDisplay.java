package railnrail.gui.simulation.dashboard.line.plan.rail;

import java.awt.Color;

/**
 * One cell describing what will be shown
 * 
 * @author Lucas SOARES
 */
public class CellDisplay
{
	/**
	 * Background color
	 */
	private Color backgroundColor = Color.WHITE;
	
	/**
	 * Build an empty cell
	 * 
	 * @param backgroundColor
	 * 		The background color
	 */
	public CellDisplay( Color backgroundColor )
	{
		// Save color
		this.backgroundColor = backgroundColor;
	}
	
	/**
	 * Build an empty cell
	 */
	public CellDisplay( )
	{
		
	}
	
	/**
	 * Get the background color
	 * 
	 * @return the background color
	 */
	public Color getBackgroundColor( )
	{
		return backgroundColor;
	}
}
