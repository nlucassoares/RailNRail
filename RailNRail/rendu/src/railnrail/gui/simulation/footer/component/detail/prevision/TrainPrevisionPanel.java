package railnrail.gui.simulation.footer.component.detail.prevision;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import railnrail.train.Train;
import railnrail.train.prevision.ArrivalPrevision;
import railnrail.train.prevision.TrainPrevisionList;

public class TrainPrevisionPanel extends JPanel
{
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -7908258805154305001L;

	/**
	 * Prevision count
	 */
	private static final int PREVISION_COUNT = 5;
	
	/**
	 * Prevision left margin
	 */
	private static final int PREVISION_LEFT_MARGIN = 15;
	
	/**
	 * Previsions
	 */
	private JLabel[ ] previsionList = new JLabel[ TrainPrevisionPanel.PREVISION_COUNT ];
	
	/**
	 * Is activated?
	 */
	private boolean isActivated = true;
	
	/**
	 * Build prevision panel
	 */
	public TrainPrevisionPanel( )
	{
		// Set layout
		setLayout( new GridLayout( 0,
				1 ) );
		
		// Set border
		setBorder( BorderFactory.createLineBorder( Color.BLACK ) );
		
		// Create prevision
		for( int i = 0; i < TrainPrevisionPanel.PREVISION_COUNT; i++ )
		{
			// Create
			previsionList[ i ] = new JLabel( );
		
			// Set background
			previsionList[ i ].setBackground( ( i % 2 ) == 0 ?
					new Color( 0x2B4EFF )
					: new Color( 0x0B0038 ) );
			previsionList[ i ].setOpaque( true );
			
			// Set font color
			previsionList[ i ].setForeground( Color.WHITE );
			
			// Set border
			previsionList[ i ].setBorder( BorderFactory.createEmptyBorder( 0,
					TrainPrevisionPanel.PREVISION_LEFT_MARGIN,
				0,
				0 ) );
			
			// Add
			add( previsionList[ i ] );
		}
	}
	
	/**
	 * Update
	 * 
	 * @param train
	 * 		The train focused
	 */
	public void update( Train train )
	{
		// Prevision list
		TrainPrevisionList prevision = train.getPrevisionList( );
		
		// Empty
		for( int i = 0; i < previsionList.length; i++ )
			previsionList[ i ].setText( "" );

		// Iterator
		int i = 0;
		
		// Activated?
		if( isActivated )
		{
			// Display arrival prevision
			while( i < prevision.getArrivalPrevision( ).size( )
					&& i < previsionList.length )
			{
				// Arrival
				ArrivalPrevision arrival = prevision.getArrivalPrevision( ).get( i );
				
				// Display arrival time
				previsionList[ i ].setText( arrival.getSection( ).getName( )
						+ ": "
						+ arrival.convertArrivalTime( )
						+ " ("
						+ arrival.getArrivalDistance( )
						+ "m)" );
				
				// Is now visible
				previsionList[ i ].setVisible( true );
				// Iterate
				i++;
			}
		}
		
		// Hide other prevision
		for( ; i < previsionList.length; i++ )
			previsionList[ i ].setVisible( false );
	}
	
	/**
	 * Activate
	 */
	public void activate( )
	{
		isActivated = true;
	}
	
	/**
	 * Deactivate
	 */
	public void deactivate( )
	{
		isActivated = false;
	}
}
