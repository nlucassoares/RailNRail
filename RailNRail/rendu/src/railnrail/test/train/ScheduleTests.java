package railnrail.test.train;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import railnrail.test.train.schedule.ScheduleTest;
import railnrail.test.train.schedule.TrainPlanningTest;

@RunWith(Suite.class)
@SuiteClasses({ ScheduleTest.class, TrainPlanningTest.class })
/**
 * TestSuite for the package train.schedule
 * @author marilou
 *
 */
public class ScheduleTests {

}
