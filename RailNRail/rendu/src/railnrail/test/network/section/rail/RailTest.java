package railnrail.test.network.section.rail;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

import railnrail.network.section.rail.Rail;
import railnrail.network.section.rail.RailDirection;
import railnrail.train.Train;
	/**
	 * 
	 * @author marilou
	 * 
	 * TestCase for class Rail of package network.section.rail
	 *
	 */
public class RailTest {

	@Test
	/**
	 * test for constructor Rail()
	 */
	public void testRail() {
		//Method automatically implemented by Eclipse
		//Unnecessary testing
	}

	@Test
	/**
	 * test for the method GetConnection()
	 */
	public void testGetConnection() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	@Test
	/**
	 * test for the method GetDirection()
	 */
	public void testGetDirection() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	@Test
	/**
	 * test for the method AddConnection()
	 */
	public void testAddConnection() 
	{
		RailDirection direction = RailDirection.RAIL_DIRECTION_ARRIVAL;
		Rail r = new Rail(0, null, direction);
		Rail r2;
		
		//r.addConnection(direction, r2);
		//assertNotNull(r.getConnection().getRail(direction, 0));
		
		for(int i = 0;i <50;i++)
		{
			
			direction = RailDirection.RAIL_DIRECTION_DEPARTURE;
			r2 = new Rail(1, null, direction);
			r.addConnection(direction, r2);
			assertNotNull(""+i+"",r.getConnection().getRail(direction, i));
			assertEquals(r2,r.getConnection().getRail(direction, i));

			direction = RailDirection.RAIL_DIRECTION_ARRIVAL;
			
			r2 = new Rail(1, null, direction);
			
			r.addConnection(direction, r2);
			assertNotNull(""+i+"",r.getConnection().getRail(direction, i));
			assertEquals(r2,r.getConnection().getRail(direction, i));
		}
	}

	@Test
	/**
	 * test for the method getParentSection()
	 */
	public void testGetParentSection() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	@Test
	/**
	 * test for the method isTrainPresent()
	 */
	public void testIsTrainPresent() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	@Test
	/**
	 * test for the method getTrain()
	 */
	public void testGetTrain() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	@Test
	/**
	 * test for the method getRailConnectedToDestination
	 */
	public void testGetRailConnectedToDestination() {
		//This method returns a method  of the class RailConnection
		//So his method is indirectly tested in the tests of the class RailConnection
	}
		
		
	@Test
	/**
	 * test  for the method enterTrain()
	 */
	public void testEnterTrain() {
		Train train = new Train(null, null, null, null, 0, 0);
		Rail r = new Rail(0, null, null);
		Random rand = new Random();
		int i = 0;
		int a = rand.nextInt(50)+1;
		if(r.getTrain() == null)
			assertTrue(r.enterTrain(train));
		if(r.getTrain()!= null)
		{
			while(r.getTrain()!= null)
			{
				
				if(i == a)
					r.leaveTrain();//, presentTrain is null
				if(i > a)
					break;
				i++;
			}
			assertNull(r.getTrain());
			assertTrue(r.enterTrain(train));
		}
		else
		{
			fail("A train is enter in rail but presentrail is null");
		}
		//If r.getTrain != null, then go to the method wait () of the thread class
		//There is no need to test it
	}

	@Test
	/**
	 * test for the method leaveTrain()
	 */
	public void testLeaveTrain() {
		Rail r = new Rail(0,null,null);
		Train train = new Train(null, null, null, null, 0, 0);
		r.enterTrain(train);
		r.leaveTrain();
		assertNull(r.getTrain());
	}

	@Test
	/**
	 * test for the method getID()
	 */
	public void testGetID() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

}
