package railnrail.simulation.incident;

import java.util.ArrayList;

import railnrail.incident.Incident;
import railnrail.incident.IncidentHolder;
import railnrail.incident.IncidentType;
import railnrail.incident.rail.RailIncident;
import railnrail.incident.rail.RailIncidentList;
import railnrail.incident.station.StationIncident;
import railnrail.incident.station.StationIncidentList;
import railnrail.incident.train.TrainIncident;
import railnrail.incident.train.TrainIncidentList;
import railnrail.network.Line;
import railnrail.network.section.Section;
import railnrail.network.section.Station;
import railnrail.network.section.rail.RailDirection;
import railnrail.simulation.Simulation;
import railnrail.simulation.time.CurrentTime;
import railnrail.train.Train;

/**
 * Incident random generator
 * 
 * @author Lucas SOARES
 */
public class IncidentGenerator
{
	/**
	 * Min delay between incident (s)
	 */
	private static final long MINIMUM_DELAY_BETWEEN_INCIDENT = 1500;
	
	/**
	 * Max delay between incident (s)
	 */
	private static final long MAXIMUM_DELAY_BETWEEN_INCIDENT = 3000;
	
	/**
	 * Current delay
	 */
	private long delay = 0;
	
	/**
	 * Time when start waiting
	 */
	private long timeStartWaiting = 0;
	
	/**
	 * Simulation
	 */
	private Simulation simulation;
	
	/**
	 * Line
	 */
	private Line line;
	
	/**
	 * Build incident generator
	 * 
	 * @param simulation
	 * 		The simulation
	 */
	public IncidentGenerator( Simulation simulation,
			Line line )
	{
		// Save
		this.simulation = simulation;
		this.line = line;
	}
	
	/**
	 * Update
	 * 
	 * @param currentTime
	 * 		The current time
	 */
	public void update( CurrentTime currentTime )
	{
		// Check
		if( delay == 0 )
		{
			// Determine delay
			delay = IncidentGenerator.MINIMUM_DELAY_BETWEEN_INCIDENT + (long)( Math.random( ) * (double)( IncidentGenerator.MAXIMUM_DELAY_BETWEEN_INCIDENT - IncidentGenerator.MINIMUM_DELAY_BETWEEN_INCIDENT ) );
		
			// Time when start to wait
			timeStartWaiting = currentTime.getCurrentTime( );
		}
		else
			if( currentTime.getCurrentTime( ) - timeStartWaiting >= delay )
			{
				// Choose one category
				IncidentType incidentType = IncidentType.values( )[ (int)( Math.random( ) * (double)IncidentType.values( ).length ) ];
				
				// Incident instance
				Incident incident;
				
				// The instance to create incident on
				IncidentHolder incidentHolder = null;
				
				// Choose one event in this category
				switch( incidentType )
				{
					default:
					case RAIL_INCIDENT:
						// Rail incident
						RailIncidentList railIncident = RailIncidentList.values( )[ (int)( Math.random( ) * (double)RailIncidentList.values( ).length ) ];
						
						// Section
						Section section;
						
						// Get section
						section = line.getSection( (int)( Math.random( ) * (double)line.getSectionCount( ) ) );
						
						// Get random rail
						incidentHolder = section.getRail( RailDirection.values( )[ (int)( Math.random( ) * (double)RailDirection.values( ).length ) ],
								(int)( Math.random( ) * (double)( section.getRailCount( ) / 2 ) ) );
						
						// Create incident
						incident = new RailIncident( simulation.getCurrentTime( ).getCurrentTime( ) + CurrentTime.INITIAL_TIMESTAMP,
								railIncident,
								incidentHolder );
						break;
						
					case STATION_INCIDENT:
						// Station incident
						StationIncidentList stationIncident = StationIncidentList.values( )[ (int)( Math.random( ) * (double)StationIncidentList.values( ).length ) ];
						
						// Section
						Section section2;
						
						// Get section
						do
						{
							section2 = line.getSection( (int)( Math.random( ) * (double)line.getSectionCount( ) ) );
						} while( !( section2 instanceof Station ) );
						
						// Save
						incidentHolder = (Station)section2;
						
						// Create incident
						incident = new StationIncident( simulation.getCurrentTime( ).getCurrentTime( ) + CurrentTime.INITIAL_TIMESTAMP,
								stationIncident,
								incidentHolder );
						break;
						
					case TRAIN_INCIDENT:
						// Train incident
						TrainIncidentList trainIncident;
						
						// Pick incident
						do
						{
							trainIncident = TrainIncidentList.values( )[ (int)( Math.random( ) * (double)TrainIncidentList.values( ).length ) ];
						// We don't want auto-train cancelation
						} while( trainIncident == TrainIncidentList.CANCELED_TRAIN );
						
						// Clone train list
						@SuppressWarnings( "unchecked" )
						ArrayList<Train> trainList = (ArrayList<Train>)simulation.getTrainList( ).clone( );
						
						// Check for train count
						if( trainList.size( ) <= 0 )
							return;
						
						// Pick up one
						incidentHolder = trainList.get( (int)( Math.random( ) * (double)trainList.size( ) ) );
						
						// Create incident
						incident = new TrainIncident( simulation.getCurrentTime( ).getCurrentTime( ) + CurrentTime.INITIAL_TIMESTAMP,
								trainIncident,
								(Train)incidentHolder );
						break;
				}
				
				// Start the incident
				incidentHolder.startIncident( incident );
				
				// Add to simulation
				simulation.addIncident( incident );
				
				// Delay reset
				delay = 0;
			}
	}
}
