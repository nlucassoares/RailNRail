package railnrail.simulation;

/**
 * Simulation states
 * 
 * @author Lucas SOARES
 */
public enum SimulationState
{
	// Paused
	SIMULATION_STATE_PAUSED,
	
	// Speeds
	SIMULATION_STATE_1,
	SIMULATION_STATE_5,
	SIMULATION_STATE_10,
	SIMULATION_STATE_50,
	SIMULATION_STATE_250,
	SIMULATION_STATE_500,
	SIMULATION_STATE_5000;
	
	/**
	 * Get speed corresponding
	 * 
	 * @param multiplier
	 * 		The speed multiplier
	 * 
	 * @return the enum value associated
	 */
	public static SimulationState getValue( int multiplier )
	{
		// Look for
		for( SimulationState s : SimulationState.values( ) )
			if( SimulationState.Value[ s.ordinal( ) ] == multiplier )
				return s;
		
		// Unknown
		return SIMULATION_STATE_PAUSED;				
	}
	
	/**
	 * Values associated
	 */
	public static final int[ ] Value =
	{
		0,
		
		1,
		5,
		10,
		50,
		250,
		500,
		5000
	};
}
