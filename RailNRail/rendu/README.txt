[RailNRail]

Simulation de la ligne L du transilien,

Par CAMPS Aur�le, LEFEVRE Marilou, MADEC Ludwig, SANTOS Gilles, SOARES Lucas.
-----------------------------------------------------------------------------

Fonctionalit�s:
   - Chargement d'un fichier CSV d�taillant les diff�rentes sections de la ligne avec pour chaque section:
	=> Un nombre de voies
	=> Un nom
	=> Une longueur (En m�tres)
	=> Une vitesse maximale (En km/h)
   - Simulation de la ligne:
	=> Gestion de la vitesse des trains, et de leurs d�placement sur les voies
	=> Gestion d'incidents g�n�r�s al�atoirement ou cr��s par l'utilisateur

Rendu :
   - Presentation et installation
	=> pour de plus amples informations
   - Manuel d'utilisation
	=> document utilisateur pour comprendre le logiciel
   - Javadoc
	=> documentation technique sur le code java
   - res
	=> fichiers techniques de configuration en csv
   - Plan des tests
	=> document technique sur les tests effectu�s
   - Avancement du projet
	=> suivi du d�roulement du projet
   - Reunion
	=> descriptif des r�unions de l'�quipe du projet
   - RailNRail
	=> fichier executable .jar