package railnrail.incident;

/**
 * Incident type
 * 
 * @author ludwigmadec
 */
public enum IncidentType
{
	RAIL_INCIDENT,
	STATION_INCIDENT,
	TRAIN_INCIDENT;
	
	/**
	 * Names
	 */
	public static final String[ ] Name =
	{
		"Rail",
		"Station",
		"Train"
	};
}
