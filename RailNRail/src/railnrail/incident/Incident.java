package railnrail.incident;

import railnrail.incident.rail.RailIncidentList;
import railnrail.incident.station.StationIncidentList;
import railnrail.incident.train.TrainIncidentList;
import railnrail.network.section.Station;
import railnrail.network.section.rail.Rail;
import railnrail.train.Train;

public abstract class Incident
{
	/**
	 * Start time
	 */
	protected long startTime;
	
	/**
	 * Type
	 */
	protected Object type;
	
	/**
	 * Object
	 */
	protected Object object;
	
	/**
	 * Incident resolution progress (per cent)
	 */
	protected double incidentResolutionProgress;
	
	/**
	 * Time elapsed
	 */
	private long timeElapsed = 0;
	
	/**
	 * Incident length (randomly calculated at start)
	 */
	protected long length;
	
	/**
	 * Is force-finished?
	 */
	private boolean isForceFinish = false;
	
	/**
	 * Build an incident
	 * 
	 * @param startTime
	 * 		The starting time
	 * @param type
	 * 		The incident type
	 * @param object
	 * 		The object to consider
	 */
	public Incident( long startTime,
			Object type,
			Object object )
	{
		// Save
		this.startTime = startTime;
		this.type = type;
		this.object = object;
	}
	
	/**
	 * Get type
	 * 
	 * @return the type
	 */
	public abstract Object getType( );
	
	/**
	 * Get object
	 * 
	 * @return the object
	 */
	public Object getObject( )
	{
		return object;
	}
	
	/**
	 * Update
	 * 
	 * @param currentTime
	 * 		The current timestamp
	 */
	public void update( long currentTime )
	{
		if( isForceFinish )
		{
			// Force end
			timeElapsed = length;
			
			// Progression is 100%
			incidentResolutionProgress = 100.0d;
		}
		else
		{
			// Calculate elapsed time
			timeElapsed = currentTime - startTime;
			
			// Update progression
			incidentResolutionProgress = ( (double)timeElapsed / (double)length ) * 100.0d;
		}
	}
	
	/**
	 * Get resolution progress
	 * 
	 * @return the progress
	 */
	public double getResolutionProgression( )
	{
		return incidentResolutionProgress;
	}
	
	/**
	 * Is passed?
	 * 
	 * @return if passed
	 */
	public boolean isPassed( )
	{
		return timeElapsed >= length;
	}
	
	/**
	 * Get length
	 * 
	 * @return the length
	 */
	public long getLength( )
	{
		return length;
	}
	
	/**
	 * Get time elapsed
	 * 
	 * @return the elapsed time
	 */
	public long getElapsedTime( )
	{
		return timeElapsed;
	}
	
	/**
	 * Get time left
	 * 
	 * @return the time left
	 */
	public long getTimeLeft( )
	{
		// Output
		long out = length - timeElapsed;
		
		// OK
		return out >= 0 ?
				out
				: 0;
	}
	
	/**
	 * Force-finish the incident
	 */
	public void finish( )
	{
		isForceFinish = true;
	}
	
	/**
	 * Is forced finish?
	 */
	public boolean isForceFinish( )
	{
		return isForceFinish;
	}
	
	/**
	 * To string
	 * 
	 * @return the string
	 */
	public String toString( )
	{
		// Output
		StringBuffer out = new StringBuffer( );
		
		// Rail incident
		if( type instanceof RailIncidentList )
		{
			// Rail incident
			out.append( "Rail " );
			out.append( ( (Rail)object ).getID( ) );
			out.append( " : " );
			out.append( RailIncidentList.Name[ ( (RailIncidentList)type ).ordinal( ) ] );
		}
		// Station incident
		else if( type instanceof StationIncidentList )
		{
			// Station incident
			out.append( ( (Station)object ).getName( ) );
			out.append( " Station" );
			out.append( " : " );
			out.append( StationIncidentList.Name[ ( (StationIncidentList)type ).ordinal( ) ] );
		}
		// Train incident
		else if( type instanceof TrainIncidentList )
		{
			// Station incident
			out.append( "Train " );
			out.append( ( (Train)object ).toString( ) );
			out.append( " : " );
			out.append( TrainIncidentList.Name[ ( (TrainIncidentList)type ).ordinal( ) ] );
		}
		
		// Incident precision
		out.append( " incident" );

		// OK
		return out.toString( );
	}
}
