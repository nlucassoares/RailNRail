package railnrail;

import java.io.IOException;

import javax.swing.JOptionPane;

import railnrail.gui.MainFrame;
import railnrail.network.Line;
import railnrail.simulation.Simulation;
import railnrail.simulation.time.CurrentTime;

/**
 * Main class
 * 
 * @author Lucas SOARES
 */
public class RailNRail
{
	/**
	 * Line name
	 */
	private static final String LINE_NAME = "LigneL.csv";

	/**
	 * Resources folder
	 */
	public static final String RES_FOLDER = "res";
	
	/**
	 * Entry point
	 * 
	 * @param args
	 * 		The arguments passed to main
	 */
	public static void main( String[ ] args )
	{
		// Line
		Line line;

		// Simulation
		Simulation simulation = new Simulation( );
		
		// Load line
		try
		{
			line = new Line( RailNRail.RES_FOLDER,
					RailNRail.LINE_NAME,
					CurrentTime.INITIAL_TIMESTAMP );
		}
		catch( IOException e )
		{
			JOptionPane.showMessageDialog( null,
			    "Can't load \""
					+ RailNRail.RES_FOLDER
					+ "/"
					+ RailNRail.LINE_NAME
					+ "\"",
			    "Loading error...",
			    JOptionPane.ERROR_MESSAGE );
			
			// Quit
			return;
		}
		
		// Create gui
		MainFrame mainGui = new MainFrame( line,
				simulation );
		
		// Launch simulation
		simulation.simulate( line,
				mainGui.getSimulationDashboard( ) );
	}
}

