package railnrail.network.empty;

import java.awt.Point;

import railnrail.network.type.EmptyCellType;

public class EmptyPlan
{
	/**
	 * Size of plan
	 */
	private Point size;
	
	/**
	 * Empty cells type
	 */
	private EmptyCellType[ ][ ] map;
	
	/**
	 * Build
	 * 
	 * @param size
	 * 		The size to build
	 */
	public EmptyPlan( Point size )
	{
		// Allocate memory
		map = new EmptyCellType[ size.x ][ ];
		for( int i = 0; i < size.x; i++ )
			map[ i ] = new EmptyCellType[ size.y ];
		
		// Randomize cases
		for( int i = 0; i < size.x; i++ )
			for( int j = 0; j < size.y; j++ )
				map[ i ][ j ] = EmptyCellType.values( )[ (int)( Math.random( ) * (double)( EmptyCellType.TYPE_4.ordinal( ) + 1 ) ) ];
		
		// Save
		this.size = size;
	}
	
	/**
	 * Get size
	 * 
	 * @return the size
	 */
	public Point getSize( )
	{
		return size;
	}
	
	/**
	 * Get cell type
	 * 
	 * @param x
	 * 		The x coordinate
	 * @param y
	 * 		The y coordinate
	 * 
	 * @return the cell empty type
	 */
	public EmptyCellType getType( int x,
			int y )
	{
		return map[ x ][ y ];
	}
}
