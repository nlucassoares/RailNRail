package railnrail.network.section.rail;

import java.util.ArrayList;

/**
 * Connections on the left and the right for each
 * rail
 * 
 * @author Lucas SOARES
 */
public class RailConnection
{
	/**
	 * Connections
	 */
	@SuppressWarnings( "unchecked" )
	private ArrayList<Rail>[ ] connectedRail = (ArrayList<Rail>[])new ArrayList[ RailDirection.values( ).length ];
	
	/**
	 * Parent rail
	 */
	private Rail parentRail;
	
	/**
	 * Construct the rail connection
	 */
	public RailConnection( Rail parentRail )
	{
		// Init the array list
		for( int i = 0; i < RailDirection.values( ).length; i++ )
			connectedRail[ i ] = new ArrayList<Rail>( );
		
		// Save
		this.parentRail = parentRail;
	}
	
	/**
	 * Add connection
	 * 
	 * @param direction
	 * 		Direction of the connection
	 * @param rail
	 * 		The rail to connect
	 */
	public void addConnection( RailDirection direction,
			Rail rail )
	{
		connectedRail[ direction.ordinal( ) ].add( rail );
	}
	
	/**
	 * Get connection count
	 * 
	 * @param direction
	 * 		The direction to analyse
	 * 
	 * @return the count
	 */
	public int getConnectionCount( RailDirection direction )
	{
		return connectedRail[ direction.ordinal( ) ].size( );
	}
	
	/**
	 * Get connection
	 * 
	 * @param direction
	 * 		The direction of the connection to get
	 * @param index
	 * 		The index of the connection
	 * 
	 * @return the rail connected
	 */
	public Rail getRail( RailDirection direction,
			int index )
	{
		try
		{
			return connectedRail[ direction.ordinal( ) ].get( index );
		}
		catch( IndexOutOfBoundsException ignored )
		{
			return null;
		}
	}
	
	/**
	 * Get rail connected to destination
	 * 
	 * @param destinationCode
	 * 		The destination code
	 * @param direction
	 * 		The direction to look at
	 * @param isLookingForFree
	 * 		Is train looking for free rail?
	 * 
	 * @return the rail which lead to this destination
	 */
	public Rail getRailConnectedToDestination( String destinationCode,
			RailDirection direction,
			boolean isLookingForFree )
	{
		// Check if we're at terminus
		if( parentRail.getParentSection( ).isTerminus( ) )
			if( parentRail.getParentSection( ).getTravelCode( ).equals( destinationCode ) )
				return parentRail;
		
		// Look for
		for( Rail r : connectedRail[ direction.ordinal( ) ] )
			if( r.getRailConnectedToDestination( destinationCode,
					direction,
					false ) != null )
				if( isLookingForFree )
				{
					if( r.isTrainPresent( ) )
						continue;
					else
						return r;
				}
				else
					return r;
			
		// Can't find
		return null;
	}
}
