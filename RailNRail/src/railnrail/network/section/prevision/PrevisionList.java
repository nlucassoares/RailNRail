package railnrail.network.section.prevision;

import railnrail.train.Train;
import railnrail.train.prevision.ArrivalPrevision;

public class PrevisionList
{
	/**
	 * Prevision count
	 */
	private static final int PREVISION_COUNT = 5;
	
	/**
	 * Prevision list
	 */
	private ArrivalPrevision[ ] prevision = new ArrivalPrevision[ PrevisionList.PREVISION_COUNT ];
	
	/**
	 * Build
	 */
	public PrevisionList( )
	{
		
	}
	
	/**
	 * Add a prevision
	 * 
	 * @param train
	 * 		The train which is arriving
	 * @param prevision
	 * 		The train prevision for this station
	 */
	public void addPrevision( Train train,
			ArrivalPrevision arrivalPrevision )
	{
		// Clear/Update
		for( int i = 0; i < PrevisionList.PREVISION_COUNT; i++ )
			// Prevision exists?
			if( prevision[ i ] != null )
			{
				// Train already in list
				if( prevision[ i ].getTrain( ) == train )
				{
					// Replace
					prevision[ i ] = arrivalPrevision;
					
					// Quit
					return;
				}
			}
		
		// Look for smaller amount of time to wait
		for( int i = 0; i < PrevisionList.PREVISION_COUNT; i++ )
			if( prevision[ i ] != null )
			{
				if( arrivalPrevision.getArrivalDelay( ) < prevision[ i ].getArrivalDelay( ) )
				{
					// Right shift
					for( int j = PrevisionList.PREVISION_COUNT - 1; j > i; j-- )
						if( j - 1 >= 0
							&& prevision[ j ] != null
							&& prevision[ j - 1 ] != null )
								prevision[ j ] = prevision[ j - 1 ];
					
					// Copy
					prevision[ i ] = arrivalPrevision;
					
					// Quit
					break;
				}
			}
			else
			{
				// Save
				prevision[ i ] = arrivalPrevision;
				
				// Quit
				break;
			}
		
		// Align
		for( int i = 0; i < PrevisionList.PREVISION_COUNT; i++ )
			if( prevision[ i ] == null )
				// Align
				for( int j = i; j < PrevisionList.PREVISION_COUNT - 1; j++ )
					prevision[ j ] = prevision[ j + 1 ];
	}
	
	/**
	 * Get prevision
	 * 
	 * @return the prevision
	 */
	public ArrivalPrevision[ ] getPrevision( )
	{
		return prevision;
	}
	
	/**
	 * Update
	 */
	public void update( )
	{
		// Check for arrival
		for( int i = 0; i < prevision.length; i++ )
			// Prevision is not empty?
			if( prevision[ i ] != null )
				// Train has passed station or arrival delay is equal 0
				if( prevision[ i ].getTrain( ).isAfter( prevision[ i ].getSection( ) )
						|| prevision[ i ].getArrivalDelay( ) <= 0 )
				{
					// Empty prevision
					prevision[ i ] = null;
				}
		
		// Check order
		for( int i = 0; i < prevision.length; i++ )
			// Prevision not null
			if( prevision[ i ] != null )
			{
				for( int j = 0; j < prevision.length; j++ )
					// i before j?
					if( i < j
						// Prevision not null
						&& prevision[ j ] != null )
						// Time inverted?
						if( prevision[ i ].getArrivalDelay( ) > prevision[ j ].getArrivalDelay( ) )
						{
							// Swap
							ArrivalPrevision tmp = prevision[ i ];
							
							// a <- b
							prevision[ i ] = prevision[ j ];
							
							// b <- a
							prevision[ j ] = tmp;
						}
			}
			// Find next no null value and replace
			else
				for( int j = 0; j < prevision.length; j++ )
					if( i < j
						&& prevision[ j ] != null )
					{
						// Replace
						prevision[ i ] = prevision[ j ];
						
						// j is now null
						prevision[ j ] = null;
						
						
						// Quit
						break;
					}
	}
}
