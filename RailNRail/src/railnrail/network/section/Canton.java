package railnrail.network.section;

import java.awt.Point;

import railnrail.network.Line;
import railnrail.network.type.RailType;

/**
 * A canton
 * 
 * @author Lucas SOARES
 */
public class Canton extends Section
{
	/**
	 * Construct a canton
	 * 
	 * @param parentLine
	 * 		The parent line
	 * @param name
	 * 		The name of the canton
	 * @param size
	 * 		The size of the canton
	 * @param travelCode
	 * 		The travel code
	 * @param id
	 * 		The section id
	 * @param isTerminus
	 * 		Is this section the last one?
	 * @param railType
	 * 		The type of rail
	 * @param position
	 * 		The position
	 * @param maximumSpeed
	 * 		The maximum speed
	 * @param nickname
	 * 		The nickname
	 */
	public Canton( Line parentLine,
			String name,
			int size,
			String travelCode,
			int id,
			boolean isTerminus,
			RailType railType,
			Point position,
			int maximumSpeed,
			String nickname )
	{
		// Parent constructor
		super( parentLine,
				name,
				size,
				travelCode,
				id,
				isTerminus,
				railType,
				position,
				maximumSpeed,
				nickname );
	}

	/**
	 * Update
	 */
	public void update( )
	{
		// Parent update
		super.update( );
	}
}
