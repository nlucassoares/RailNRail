package railnrail.train;

import railnrail.incident.Incident;
import railnrail.incident.IncidentHolder;
import railnrail.incident.train.TrainIncident;
import railnrail.incident.train.TrainIncidentList;
import railnrail.math.speed.Helper;
import railnrail.network.section.Section;
import railnrail.network.section.rail.Rail;
import railnrail.network.section.rail.RailDirection;
import railnrail.simulation.Simulation;
import railnrail.train.prevision.TrainPrevisionList;

/**
 * Train represented by a thread, containing self schedule previsions
 * 
 * @author Lucas SOARES
 */
public class Train extends Thread
	implements IncidentHolder
{
	/**
	 * Simulation
	 */
	private Simulation simulation;
	
	/**
	 * Rail train is currently on
	 */
	private Rail currentRail = null;
	
	/**
	 * Code for the destination
	 */
	private String travelCode;
	
	/**
	 * Current speed (km/h)
	 */
	private double currentSpeed;

	/**
	 * Current position on rail
	 */
	private double currentPosition;
	
	/**
	 * Is running?
	 */
	private boolean isRunning = true;
	
	/**
	 * Is must quit?
	 */
	private boolean isMustQuit = false;
	
	/**
	 * Speed multiplier
	 */
	private int speedMultiplier;
	
	/**
	 * Last update (ms)
	 */
	private long lastUpdate;
	
	/**
	 * Direction
	 */
	private RailDirection direction;
	
	/**
	 * Unique id
	 */
	private int id;
	
	/**
	 * Train prevision
	 */
	private TrainPrevisionList trainPrevisionList;
	
	/**
	 * First train prevision list
	 */
	private TrainPrevisionList firstTrainPrevisionList;
	
	/**
	 * Late time
	 */
	private long lateTime = 0;
	
	/**
	 * Incident
	 */
	private TrainIncident incident;
	
	/**
	 * Is blocked?
	 */
	private boolean isBlocked = false;
	
	/**
	 * Construct a train
	 * 
	 * @param travelCode
	 * 		The travel code
	 * @param startingRail
	 * 		What rail the train start from
	 * @param direction
	 * 		Direction to go
	 * @param speedMultiplier
	 * 		Current speed multiplier
	 * @param id
	 * 		Train UUID
	 */
	public Train( Simulation simulation,
			String travelCode,
			Rail startingRail,
			RailDirection direction,
			int speedMultiplier,
			int id )
	{
		// Save
		this.simulation = simulation;
		this.travelCode = travelCode;
		this.currentRail = startingRail;
		this.speedMultiplier = speedMultiplier;
		this.direction = direction;
		this.id = id;
		
		// Waiting for simulation to start
		isRunning = false;
		trainPrevisionList = null;
	}
	
	/**
	 * Get current position
	 * 
	 * @return the position
	 */
	public double getPosition( )
	{
		return currentPosition;
	}
	
	/**
	 * Run the train
	 */
	@Override
	public void run( )
	{
		// Enter the first rail
		currentRail.enterTrain( this );
		
		// First update
		lastUpdate = System.currentTimeMillis( );
		
		do
		{
			// Is simulation running?
			if( isRunning
					&& !isMustQuit )
			{
				// Time diff
				long deltaUpdate = ( System.currentTimeMillis( ) - lastUpdate );
				
				// Increment position
				if( deltaUpdate > 0 )
					currentPosition += ( ( 1.0d / ( (double)deltaUpdate * 1000.0d ) ) * Helper.km_hToM_s( currentSpeed ) ) * speedMultiplier;
				
				// Save last update
				lastUpdate = System.currentTimeMillis( );
				
				// Check current position
				if( (int)currentPosition >= currentRail.getParentSection( ).getSize( ) )
				{
					// Next rail to enter on
					Rail nextRail;
					
					// Maximize position
					currentPosition = currentRail.getParentSection( ).getSize( );
					
					// Get next rail
					if( ( nextRail = currentRail.getRailConnectedToDestination( travelCode,
							direction,
							true ) ) == null )
						if( ( nextRail = currentRail.getRailConnectedToDestination( travelCode,
								direction,
								false ) ) == null )
							continue;
					
					// Next rail is current rail equals terminus
					if( nextRail == currentRail )
					{
						// Leave the rail
						currentRail.leaveTrain( );

						// We must now quit
						isMustQuit = true;
						
						// Continue...
						continue;
					}
					
					// Enter next rail
					nextRail.enterTrain( this );
					
					// Exit previous rail
					currentRail.leaveTrain( );
					
					// Save current rail
					currentRail = nextRail;
				}
			}
			
			// Incident occuring?
			if( incident != null )
			{
				// Check for incident end
				if( incident.isPassed( ) )
					// No more incident
					incident = null;
				else
					// Speed is now 0
					currentSpeed = 0;
				
				// Save last update
				lastUpdate = System.currentTimeMillis( );
			}
			// No incident
			else
				// Set current speed
				currentSpeed = currentRail.getParentSection( ).getMaximumSpeed( );
			
			// If blocked
			if( isBlocked )
			{
				// Set current speed
				currentSpeed = 0;
				
				// Save last update
				lastUpdate = System.currentTimeMillis( );
			}
			
			// Sleep
			try
			{
				Thread.sleep( 1 );
			}
			catch( InterruptedException e )
			{
			}
		} while( !isMustQuit
				&& ( !currentRail.getParentSection( ).isTerminus( )
						|| currentRail.getParentSection( ).getTravelCode( ) != travelCode ) );
	}
	
	/**
	 * Quit and leave the train as it is
	 */
	public void quit( )
	{
		// Thread must exit
		isMustQuit = true;
		
		// Force interrupt
		interrupt( );
	}
	
	/**
	 * Resume simulation
	 */
	public void resumeSimulation( )
	{
		// Is now running
		isRunning = true;
		
		// Save new last update
		lastUpdate = System.currentTimeMillis( );
	}
	/**
	 * Get the last update
	 * 
	 * @return the last update
	 */
	public long getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * Change simulation speed multiplier
	 * 
	 * @param factor
	 * 		The new multiplier speed
	 */
	public void setSpeedMultiplier( int multiplier )
	{
		// Save
		speedMultiplier = multiplier;
	}
	
	/**
	 * Pause simulation
	 */
	public void pauseSimulation( )
	{
		// Not running anymore
		isRunning = false;
	}
	
	/**
	 * Get rail train is on
	 * 
	 * @return the rail
	 */
	public Rail getCurrentRail( )
	{
		return currentRail;
	}
	
	/**
	 * Get id
	 * 
	 * @return the id
	 */
	public int getTrainId( )
	{
		return id;
	}
	
	/**
	 * Get direction
	 * 
	 * @return the direction
	 */
	public RailDirection getDirection( )
	{
		return direction;
	}
	
	/**
	 * Get the travel code
	 * 
	 * @return the travel code
	 */
	public String getTravelCode( )
	{
		return travelCode;
	}
	
	/**
	 * Get current speed
	 * 
	 * @return the current speed
	 */
	public double getCurrentSpeed( )
	{
		return currentSpeed;
	}
	
	/**
	 * Get prevision
	 * 
	 * @return the prevision
	 */
	public TrainPrevisionList getPrevisionList( )
	{
		return trainPrevisionList;
	}
	
	/**
	 * Get late time
	 * 
	 * @return late time
	 */
	public long getLateTime( )
	{
		return lateTime;
	}
	
	/**
	 * Set at rail start
	 */
	public void setAtRailStart( )
	{
		currentPosition = 0;
	}
	
	/**
	 * Is train arrived at destination?
	 * 
	 * @return if train is arrived
	 */
	public boolean isArrived( )
	{
		return isMustQuit;
	}
	
	/**
	 * Update arrival time
	 */
	public void updateArrivalTime( )
	{
		// Check
		if( isArrived( ) )
			return;
		
		// Save if first/exists
		if( trainPrevisionList != null )
		{
			if( firstTrainPrevisionList == null )
			{
				if( trainPrevisionList.getArrivalPrevision( ).size( ) > 0 )
					firstTrainPrevisionList = trainPrevisionList;
			}
			else
				if( trainPrevisionList.getArrivalPrevision( ).size( ) > 0 )
					lateTime = trainPrevisionList.getArrivalPrevision( ).get( trainPrevisionList.getArrivalPrevision( ).size( ) - 1 ).getArrivalTime( ) - firstTrainPrevisionList.getArrivalPrevision( ).get( firstTrainPrevisionList.getArrivalPrevision( ).size( ) - 1 ).getArrivalTime( );
		}
		
		// Calculate previsions
		trainPrevisionList = new TrainPrevisionList( this,
				simulation );
	}
	
	/**
	 * Is after or on this section?
	 * 
	 * @param section
	 * 		The section to be after to return true
	 * 
	 * @return if train is after
	 */
	public boolean isAfter( Section section )
	{
		// Current rail
		Rail currentRail = this.currentRail;
		
		// Check if first
		if( currentRail.getParentSection( ) == section )
			return true;
		
		// Check
		do
		{
			// Get next rail
			currentRail = currentRail.getRailConnectedToDestination( travelCode,
					direction,
					false );
			
			// Check
			if( currentRail.getParentSection( ) == section )
				return false;
		} while( !currentRail.getParentSection( ).isTerminus( ) );
		
		// We're after
		return true;
	}
	
	/**
	 * To string
	 * 
	 * @return the string version of this object
	 */
	public String toString( )
	{
		return travelCode
			+ " ("
			+ id
			+ ")";
	}
	/**
	 *  Is train running ?
	 * @return if Train is running
	 */
	public boolean isRunning() {
		return isRunning;
	}

	/**
	 * Start an incident
	 * 
	 * @param incident
	 *		The incident
	 */
	@Override
	public void startIncident( Incident incident )
	{
		// Save
		this.incident = (TrainIncident)incident;
		
		// Check for special incident
		if( ( (TrainIncidentList)incident.getType( ) ) == TrainIncidentList.CANCELED_TRAIN )
		{
			// Interrupt thread
			interrupt( );
			
			// Not running
			isRunning = false;
						
			// Train is arrived
			isMustQuit = true;
						
			// Train leave the rail
			if( currentRail != null )
				currentRail.leaveTrain( );
		}
	}
	
	/**
	 * Is an incident occuring?
	 * 
	 * @return if incident is occuring
	 */
	public boolean isIncidentOccuring( )
	{
		return incident != null;
	}
	
	/**
	 * Get incident
	 * 
	 * @return the incident occuring
	 */
	public TrainIncident getIncident( )
	{
		return incident;
	}
	
	/**
	 * Set blocked
	 * 
	 * @param isBlocked
	 * 		Is train blocked?
	 */
	public void setBlocked( boolean isBlocked )
	{
		this.isBlocked = isBlocked;
	}
	
	/**
	 * Is blocked?
	 * 
	 * @return if blocked
	 */
	public boolean isBlocked( )
	{
		return isBlocked;
	}
}
