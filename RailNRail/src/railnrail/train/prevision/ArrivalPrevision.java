package railnrail.train.prevision;

import java.util.Date;

import railnrail.network.section.Section;
import railnrail.simulation.time.CurrentTime;
import railnrail.simulation.time.Helper;
import railnrail.train.Train;

/**
 * One arrival prevision for one train
 * 
 * @author SOARES Lucas
 */
public class ArrivalPrevision
{
	/**
	 * Station
	 */
	private Section section;
	
	/**
	 * Time of arrival
	 */
	private long arrivalDelay;
	
	/**
	 * Timestamp at calculation
	 */
	private long currentTimestamp;
	
	/**
	 * Arrival distance
	 */
	private int arrivalDistance;
	
	/**
	 * Train concerned
	 */
	private Train train;
	
	/**
	 * Construct
	 * 
	 * @param section
	 * 		The station where to arrive
	 * @param train
	 * 		The train concerned
	 * @param arrivalDelay
	 * 		The arrival delay
	 * @param currentTimestamp
	 * 		The current timestamp
	 * @param arrivalDistance
	 * 		The arrival distance
	 */
	public ArrivalPrevision( Section section,
			Train train,
			long arrivalDelay,
			long currentTimestamp,
			int arrivalDistance )
	{
		// Save
		this.section = section;
		this.train = train;
		this.arrivalDelay = arrivalDelay;
		this.currentTimestamp = currentTimestamp;
		this.arrivalDistance = arrivalDistance;
	}

	/**
	 * @return the section
	 */
	public Section getSection( )
	{
		return section;
	}
	
	/**
	 * @return the train
	 */
	public Train getTrain( )
	{
		return train;
	}

	/**
	 * @return the arrival delay
	 */
	public long getArrivalDelay( )
	{
		return arrivalDelay;
	}

	/**
	 * @return the current timestamp
	 */
	public long getCurrentTimestamp( )
	{
		return currentTimestamp;
	}
	
	/**
	 * @return the arrival distance
	 */
	public int getArrivalDistance( )
	{
		return arrivalDistance;
	}
	
	/**
	 * Convert time to arrive into string
	 * 
	 * @return the arrival converted time
	 */
	public String convertArrivalTime( )
	{
		return Helper.convertSecondToText( arrivalDelay );
	}
	
	/**
	 * Get arrival time
	 * 
	 * @return the arrival time
	 */
	public long getArrivalTime( )
	{
		return CurrentTime.INITIAL_TIMESTAMP + arrivalDelay + currentTimestamp;
	}
	
	/**
	 * Get arrival date
	 * 
	 * @return the arrival date
	 */
	public Date getArrivalDate( )
	{
		return new Date( getArrivalTime( ) );
	}
}
