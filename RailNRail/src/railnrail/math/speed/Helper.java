package railnrail.math.speed;

/**
 * Speed gesture helper
 * 
 * @author Lucas SOARES
 */
public class Helper
{
	/**
	 * Constant to convert km/h to m/s
	 */
	private static final double KM_H_TO_M_S = 0.277778;
	
	/**
	 * Constant to convert m/s to km/h
	 */
	private static final double M_S_TO_KM_H = 3.6;
	
	/**
	 * Convert km/h to m/s
	 * 
	 * @param kmh
	 * 		The km/h value
	 * 
	 * @return the conversion result
	 */
	public static double km_hToM_s( double kmh )
	{
		return kmh * KM_H_TO_M_S;
	}
	
	/**
	 * Convert m/s to km/h
	 * 
	 * @param ms
	 * 		The m/s value
	 * 
	 * @return the converted result
	 */
	public static double m_sToKm_h( double ms )
	{
		return ms * M_S_TO_KM_H;
	}
}
