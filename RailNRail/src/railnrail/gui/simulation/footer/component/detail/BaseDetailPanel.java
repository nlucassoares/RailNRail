package railnrail.gui.simulation.footer.component.detail;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * Base panel inherited by all detailed panel in the footer
 * 
 * @author Lucas SOARES
 */
public abstract class BaseDetailPanel extends JPanel
{
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 6881987086096031537L;
	
	/**
	 * Title size
	 */
	public static final int FONT_SIZE_TITLE = 20;
	
	/**
	 * Detail panel
	 */
	protected DetailPanel detailPanel;
	
	/**
	 * Construct base detail panel
	 * 
	 * @param detailPanel
	 * 		The detail panel
	 */
	public BaseDetailPanel( DetailPanel detailPanel )
	{
		// Parent constructor
		super( );
		
		// Save
		this.detailPanel = detailPanel;
		
		// Set layout
		setLayout( new BorderLayout( ) );
		
		// Set border
		setBorder( BorderFactory.createCompoundBorder( BorderFactory.createEmptyBorder( 0,
					10,
					10,
					0 ),
				BorderFactory.createCompoundBorder( BorderFactory.createLineBorder( Color.GRAY ),
						BorderFactory.createEmptyBorder( 5, 0, 5, 0 ) ) ) );
	}

	/**
	 * Update
	 */
	public abstract void update( );
	
	/**
	 * Activate
	 */
	public abstract void activate( );
	
	/**
	 * Deactivate
	 */
	public abstract void deactivate( );
}
