package railnrail.gui.simulation.incident;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import railnrail.incident.Incident;
import railnrail.incident.IncidentHolder;
import railnrail.incident.IncidentType;
import railnrail.incident.rail.RailIncident;
import railnrail.incident.rail.RailIncidentList;
import railnrail.incident.station.StationIncident;
import railnrail.incident.station.StationIncidentList;
import railnrail.incident.train.TrainIncident;
import railnrail.incident.train.TrainIncidentList;
import railnrail.res.img.ImageResource;
import railnrail.simulation.Simulation;
import railnrail.simulation.time.CurrentTime;

public class IncidentBuilder
{
	/**
	 * Main frame
	 */
	private JFrame mainFrame;
	
	/**
	 * Simulation
	 */
	private Simulation simulation;
	
	/**
	 * Build dialog
	 * 
	 * @param mainFrame
	 * 		The frame which dialog depends
	 * @param simulation
	 * 		The simulation
	 */
	public IncidentBuilder( JFrame mainFrame,
			Simulation simulation )
	{
		// Save
		this.mainFrame = mainFrame;
		this.simulation = simulation;
	}
	
	/**
	 * Create incident
	 * 
	 * @param type
	 * 		The incident type
	 * @param object
	 * 		The object to create incident on
	 */
	public void createIncident( IncidentType type,
			IncidentHolder object )
	{
		// Possibilities
		String[ ] possibilities = null;
		
		// Result
		String result;

		// Generate possibilities
		switch( type )
		{
			case RAIL_INCIDENT:
				possibilities = RailIncidentList.Name;
				break;
				
			case STATION_INCIDENT:
				possibilities = StationIncidentList.Name;
				break;
				
			case TRAIN_INCIDENT:
				possibilities = TrainIncidentList.Name;
				break;
				
			default:
				return;
		}
				
		try
		{
			// Wait for user to choose
			result = (String)JOptionPane.showInputDialog( mainFrame,
				"Choose the incident on "
				+ IncidentType.Name[ type.ordinal( ) ],
				"Incident creation",
				JOptionPane.PLAIN_MESSAGE,
				ImageResource.loadIcon( ImageResource.IMAGE_INCIDENT_ICON ),
				possibilities,
				possibilities[ 0 ] );
		}
		catch( Exception e )
		{
			return;
		}

		// Incident
		Incident incident = null;
		
		// Process with selection
		if( result != null
				&& result.length() > 0 )
			switch( type )
			{
				case RAIL_INCIDENT:
					// Start incident
					object.startIncident( incident = new RailIncident( simulation.getCurrentTime( ).getCurrentTime( ) + CurrentTime.INITIAL_TIMESTAMP,
							RailIncidentList.getValue( result ),
							object ) );
					break;
				
				case STATION_INCIDENT:
					// Start incident
					object.startIncident( incident = new StationIncident( simulation.getCurrentTime( ).getCurrentTime( ) + CurrentTime.INITIAL_TIMESTAMP,
							StationIncidentList.getValue( result ),
							object ) );
					break;
					
				case TRAIN_INCIDENT:
					// Start incident
					object.startIncident( incident = new TrainIncident( simulation.getCurrentTime( ).getCurrentTime( ) + CurrentTime.INITIAL_TIMESTAMP,
							TrainIncidentList.getValue( result ),
							object ) );
					break;
					
				default:
					return;
			}
				
		// Add incident in incident list
		if( incident != null )
			simulation.addIncident( incident );
	}
	
}
