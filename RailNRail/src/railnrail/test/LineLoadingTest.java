package railnrail.test;

import java.io.IOException;

import railnrail.network.Line;
import railnrail.network.section.rail.Rail;
import railnrail.network.section.rail.RailDirection;

/**
 * Line loading test
 * 
 * @author Lucas SOARES
 */
public class LineLoadingTest
{
	/**
	 * Line name
	 */
	private static final String LINE_NAME = "LigneL.csv";
	
	/**
	 * Res folder
	 */
	private static final String RES_FOLDER = "res";
	
	/**
	 * Load line test
	 * 
	 * @return if test succeeded
	 */
	public static void loadTest( ) throws TestFailedException
	{
		// Line
		Line line;
		
		// Load line
		try
		{
			line = new Line( LineLoadingTest.RES_FOLDER,
					LineLoadingTest.LINE_NAME,
					0 );
		}
		catch( IOException e )
		{
			throw new TestFailedException( );
		}
		
		// Get first rail from start
		Rail r = line.getSection( 0 ).getRail( RailDirection.RAIL_DIRECTION_DEPARTURE,
			0 );
		
		// Display path for line
		System.out.println( "Line 0 from st-lazare: " );
		while( r != null )
		{
			System.out.println(  r.getParentSection( ).getName( ) );
			r = r.getConnection( ).getRail( RailDirection.RAIL_DIRECTION_DEPARTURE,
				0 );
		}
		
		// Get last section (at the st nom la breteche, terminus)
		r = line.getSection( line.getSectionCount( ) - 1 ).getRail( RailDirection.RAIL_DIRECTION_ARRIVAL,
			0 );
		
		// Display reverse path for line
		System.out.println( "\nReverse line from Saint nom la bret�che: \n" );
		while( r != null )
		{
			System.out.println(  r.getParentSection( ).getName( ) );
			r = r.getConnection( ).getRail( RailDirection.RAIL_DIRECTION_ARRIVAL,
				0 );
		}
		
		// Destination code test
		System.out.println( "\nDestination code test, we wan't to go to Versailles rive droite from st lazare:\n" );
		r = line.getSection( 0 ).getRail(  RailDirection.RAIL_DIRECTION_DEPARTURE,
				0 );
		do
		{
			System.out.println( r.getParentSection( ).getName( ) );
			r = r.getRailConnectedToDestination( "DVRD",
					RailDirection.RAIL_DIRECTION_DEPARTURE,
					false );
		} while( r != null
				&& !r.getParentSection( ).isTerminus( ) );
		
		System.out.println( r.getParentSection( ).getName( ) );
		
		System.out.println( "\nDestination code test, we wan't to go to St Lazare from Cergy le haut:\n" );
		r = line.getSection( "Cergy le Haut" ).getRail(  RailDirection.RAIL_DIRECTION_ARRIVAL,
				0 );
		do
		{
			System.out.println( r.getParentSection( ).getName( ) );
			r = r.getRailConnectedToDestination( "DSLZ",
					RailDirection.RAIL_DIRECTION_ARRIVAL,
					false );
		} while( r != null
				&& !r.getParentSection( ).isTerminus( ) );
		System.out.println( r.getParentSection( ).getName( ) );
		
		// Rail connections display
		System.out.println(  "\nRail connection test: " );
		for( RailDirection d : RailDirection.values( ) )
			for( int i = 0; i < line.getSection( "B�con les bruy�res" ).getRailCount( d ); i++ )
				for( int j = 0; j < line.getSection( "B�con les bruy�res" ).getRail( d ).get( i ).getConnection( ).getConnectionCount( d ); j++ )
				System.out.println( i
						+ ", "
						+ j
						+ ": "
						+ line.getSection( "B�con les bruy�res" ).getRail( d ).get( i ).getConnection( ).getRail( d, j ).getParentSection( ).getName( )
						+ " direction("
						+ d
						+ ")" );
	}
}
