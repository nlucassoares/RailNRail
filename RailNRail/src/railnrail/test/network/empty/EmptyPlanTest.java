/**
 * 
 */
package railnrail.test.network.empty;

import static org.junit.Assert.*;

import java.awt.Point;

import org.junit.Test;

import railnrail.network.empty.EmptyPlan;
import railnrail.network.type.EmptyCellType;

/**
 * @author marilou
 *
 */
public class EmptyPlanTest {

	/**
	 *Test method for EmptyPlan() of class EmptyPlans
	 */
	@Test
	public void testEmptyPlan() {
			Point size = new Point(50,50);
			EmptyPlan ep = new EmptyPlan(size);

		for( int i = 0; i < size.x; i++ )
			for( int j = 0; j < size.y; j++ )
				assertTrue(ep.getType(i, j) instanceof EmptyCellType );
		assertEquals(ep.getSize(),size);
	}

	/**
	 * Test method for {@link railnrail.network.empty.EmptyPlan#getSize()}.
	 */
	@Test
	public void testGetSize() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

	/**
	 * Test method for {@link railnrail.network.empty.EmptyPlan#getType(int, int)}.
	 */
	@Test
	public void testGetType() {
		//Method automatically implemented by Eclipse
				//Unnecessary testing
	}

}
