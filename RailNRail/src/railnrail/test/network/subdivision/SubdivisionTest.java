package railnrail.test.network.subdivision;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.junit.Test;

import railnrail.network.subdivision.Subdivision;
/**
 * 
 * @author marilou
 * TestCase for the class Subdivision of the package network.subdivision
 * 
 */
public class SubdivisionTest{

	@Test
	/**
	 * test for the constructor Subdivision()
	 */
	public void testSubdivision() throws IOException {
		Random r = new Random();
		String str = "" + r.nextInt(10) + "-" + r.nextInt(10) + ",";
		int k = r.nextInt(10);
		for(int j = 0; j < k ; j++)
		{
			str = str + r.nextInt(10) + "-" + r.nextInt(10) + ",";
		}
		Subdivision subdivision = new Subdivision(str);
		ArrayList<Integer> begin = new ArrayList<Integer>();
		ArrayList<Integer> end = new ArrayList<Integer>();
		
		String[ ] properties = str.split( "," );
		
		for(String division : properties)
		{
			String [ ] limit = division.split("-");
			begin.add(Integer.parseInt(limit[0]));
			end.add(Integer.parseInt(limit[1]));
		}
		
		for(int i = 0; i < begin.size();i++)
		{
			assertEquals(begin.get(i),(Integer)subdivision.getSubdivisionLimit(i).getBeginID());
			assertEquals(end.get(i),(Integer)subdivision.getSubdivisionLimit(i).getEndID());
		}
	}

	@Test
	/**
	 * test for the method getSubdivisionLimitCount()
	 */
	public void testGetSubdivisionLimitCount() {
		//Method automatically implemented by Eclipse
		//Unnecessary testing
	}

	@Test
	/**
	 * test for the method getSubvisionLimit()
	 */
	public void testGetSubdivisionLimit() {
		//Method automatically implemented by Eclipse
		//Unnecessary testing
	}

}
