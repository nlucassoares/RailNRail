package railnrail.test;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

import railnrail.network.section.Canton;
import railnrail.network.section.Section;
import railnrail.network.section.rail.Rail;
import railnrail.network.section.rail.RailDirection;
import railnrail.network.type.RailType;
import railnrail.simulation.Simulation;
import railnrail.train.Train;

public class TrainIntegrationTest {
	
	
	@Test
	public void test() {
		Train train1;
		Train train2;
		Simulation simulation = null;
		Section canton = new Canton(null, "Canton Parent", 300, "01", 0, false, RailType.RAIL_HORIZONTAL, null, 0, "Section 1");
		Rail startingRail = new Rail(0, canton, RailDirection.RAIL_DIRECTION_ARRIVAL);
		RailDirection direction = RailDirection.RAIL_DIRECTION_ARRIVAL;
		Random r = new Random();
		TestThread test = new TestThread();
		
		train1 = new TrainTestHerited(simulation, "Train1", startingRail, direction, r.nextInt(), 1);
		train2 = new TrainTestHerited(simulation, "Train2", startingRail, direction, r.nextInt(), 2);
		train1.start();
		train2.start();
		test.start();
		
		while(train1.isAlive() && train2.isAlive() )
		{
			if(test.isInterrupted())
			{
				fail("Error");
			}
		}
		
		
			
		
	}

}

class TestThread extends Thread
{
	public synchronized void run()
	{

		try {
			wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.interrupt();
			
	}
}

class TrainTestHerited extends Train
{
	/**
	 * Rail train is currently on
	 */
	private Rail currentRail = null;
	

	public TrainTestHerited(Simulation simulation, String travelCode, Rail startingRail, RailDirection direction,
			int speedMultiplier, int id) {
		super(simulation, travelCode, startingRail, direction, speedMultiplier, id);
		this.currentRail = super.getCurrentRail();
		// TODO Auto-generated constructor stub
	}
	
	public synchronized void run()
	{
		if(super.getId() == 1)
				this.notify();
		if(super.getId() == 2)
		{
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			assertNotNull(currentRail.getTrain());
		}
		
		assertTrue(currentRail.enterTrain(this));
		assertNotNull(currentRail.getTrain());
		
		if(super.getId() == 1)
			this.notify();
		
		currentRail.leaveTrain();
		
		
		if(super.getId() == 2)
		{
			assertNull(currentRail.getTrain());
		}
		else if(super.getId() == 1)
		{
			assertNotNull(currentRail.getTrain());
		}
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}