package railnrail.test.math.speed;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

import railnrail.math.speed.Helper;
/**
 * TestCase for the class Helper of the package math.speed
 * @author marilou
 *
 */
public class HelperTest {

	@Test
	/**
	 * test for the method Km_hToM_s()
	 */
	public void testKm_hToM_s() {
		double K = 0.277778;
		double km;
		Random r = new Random();
		assertEquals(K, Helper.km_hToM_s(1),0);
		
		for(int i = 0; i < 50; i++)
		{
			km = r.nextDouble();
			assertEquals((K*km), Helper.km_hToM_s(km),0);
		}
	}

	@Test
	/**
	 * test for the method M_sToKm_h()
	 */
	public void testM_sToKm_h() {
		double M = 3.6;
		double ms;
		Random r = new Random();
		assertEquals((M), Helper.m_sToKm_h(1),0);
		for(int i = 0; i < 50; i++)
		{
			ms = r.nextDouble();
			assertEquals((M*ms), Helper.m_sToKm_h(ms),0);
		}
		
	}

}
