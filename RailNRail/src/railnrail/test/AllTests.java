package railnrail.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ FileTests.class, MathSpeedTests.class, NetworkTests.class, TrainIntegrationTest.class, TrainTests.class,
		UnitTests.class, LineTest.class })
public class AllTests {

}
